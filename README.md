## Test Case

|Test Case ID|Test Ojective |Precondition|Steps:|Test Data|Expected result|
|:-----------|:-------------|:-----------|:-----|:--------|:--------------|
|TC1|Managers can view the list of training points ![Screenshot_2023-10-24_205133](/uploads/7dc61e19da938437b9df9bd755282375/Screenshot_2023-10-24_205133.png)|User is logged into the system|1.Open the application  2. Enter the student code to search  3. Chose Check button|Check for student training points|Users can view the list of training points, then edit and saves the edited data to the database|

